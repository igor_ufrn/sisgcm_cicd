Projeto com exemplo de uso da tecnologia Spring e Gitlab CICD
com as seguintes fases:

- testes_unidade
- testes_integracao
- testes_api
- quality_gate_sonar
- build_spring_jar
- build-imagem_aplicacao
- autoriza_deploy_producao
- deploy-project
- testes_performance
