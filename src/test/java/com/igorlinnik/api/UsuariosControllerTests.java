package com.igorlinnik.api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsuariosControllerTests {

    @LocalServerPort
    private int port;

    private static final String CLIENT_ID = "sisgcm";
    private static final String CLENT_SECRET = "123";
    private static final String USER_NAME = "f625982";
    private static final String PASSWORD = "1";
    private static final String BASE_URI = "http://servidorautorizacao:8063";


    private static String getAuthorization() throws JSONException {
        Response response = RestAssured.given()
                .log().all()
                .auth()
                .preemptive()
                .basic(CLIENT_ID, CLENT_SECRET)
                .contentType("application/x-www-form-urlencoded")
                .formParam("grant_type", "password")
                .formParam("username", USER_NAME)
                .formParam("password", PASSWORD)
            .when()
                .post(BASE_URI+"/oauth/token");

        JSONObject jsonObject = new JSONObject(response.getBody().asString());
        String accessToken = jsonObject.get("access_token").toString();
        String tokenType = jsonObject.get("token_type").toString();
        return tokenType + " " + accessToken;
    }


    @Test
    public void testarLogin() throws JSONException {
        String autorizacao = getAuthorization();
        Assertions.assertNotNull(autorizacao);
    }

    @Test
    public void listarUsuarios() throws JSONException {
        RestAssured
            .given()
                .basePath("/usuarios/listar")
                .port(port)
                .header("Authorization", getAuthorization())
                .accept(ContentType.JSON)
            .when()
                .get()
            .then()
                .statusCode(HttpStatus.OK.value());
    }


}
