package com.igorlinnik.integracao;


import com.igorlinnik.dominio.Usuario;
import com.igorlinnik.services.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolationException;

/**
 * A anotação @SpringBootTest fornece as capacidades do Spring para executar testes.
 * Por exemplo, injeção de dependências para permitir testes de integração.
 */
@SpringBootTest
public class UsuarioServiceTests {

    @Autowired
    private UsuarioService usuarioService;

    @Test
    void verificarLoginExistente() {
        Assertions.assertNotNull(usuarioService.getUsuarioByLogin("f625982"));
    }

    @Test
    void cadastrarUsuarioFaltandoDadosObrigatorios() {
        Usuario usuario = Usuario.builder().build();
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            usuarioService.cadastrar(usuario);
        });
    }


}
