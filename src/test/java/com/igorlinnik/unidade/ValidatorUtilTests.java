package com.igorlinnik.unidade;

import com.igorlinnik.validation.ValidatorUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ValidatorUtilTests {

    @Test
    void verificaLoginValido() { Assertions.assertTrue(ValidatorUtil.isLoginValido("f625982")); }

    @Test
    void verificaLoginVazioInvalido() {
        Assertions.assertFalse(ValidatorUtil.isLoginValido(""));
    }

    @Test
    void verificaLoginNuloInvalido() {
        Assertions.assertFalse(ValidatorUtil.isLoginValido(null));
    }



}
