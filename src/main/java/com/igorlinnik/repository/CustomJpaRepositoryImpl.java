package com.igorlinnik.repository;

import org.hibernate.Session;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Date;


public class CustomJpaRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> implements CustomJpaRepository<T, ID> {

    private EntityManager entityManager;

    public CustomJpaRepositoryImpl(JpaEntityInformation<T, ?> entityInformation,
                                   EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public void detach(T entity) {
        entityManager.detach(entity);
    }


    public String parseString(Object obj){
        if(obj == null)
            return null;

        if(obj instanceof String){
            return (String)obj;
        }else if(obj instanceof Character){
            return ((Character)obj).toString();
        }else if(obj instanceof Clob){
            Clob clob = (Clob) obj;
            try {
                return clob.getSubString(1, Long.valueOf(clob.length()).intValue() );
            } catch (SQLException e) {
                return null;
            }
        }

        return null;
    }

    public Character parseCharacter(Object obj){
        if(obj == null){
            return null;
        }

        if(obj instanceof String){
            return ((String)obj).charAt(0);
        }else if(obj instanceof Character){
            return (Character)obj;
        }

        return Character.valueOf(' ');
    }

    public Long parseLong(Object obj){
        if(obj != null){
            if(obj instanceof BigDecimal)
                return ((BigDecimal)obj).longValue();
            else
                return (Long)obj;
        }

        return Long.valueOf(0);
    }

    public Short parseShort(Object obj){
        if(obj != null){
            if(obj instanceof BigDecimal)
                return ((BigDecimal)obj).shortValue();
            else
                return (Short)obj;
        }

        return (short)0;
    }

    public Double parseDouble(Object obj){
        if(obj != null){
            if(obj instanceof BigDecimal)
                return ((BigDecimal)obj).doubleValue();
            else
                return (Double)obj;
        }

        return Double.valueOf(0);
    }

    public Integer parseInteger(Object obj){
        if(obj != null){
            if(obj instanceof BigDecimal)
                return ((BigDecimal)obj).intValue();
            else if(obj instanceof Long)
                return ((Long)obj).intValue();
            else
                return (Integer)obj;
        }

        return Integer.valueOf(0);
    }

    public Date parseDate(Object obj){
        return obj != null ? (Date)obj : null;
    }

    public Boolean parseBoolean(Object obj){
        if(obj != null){
            if(obj instanceof Number)
                return obj.toString().equals("1") ? true : false;
            else
                return (Boolean)obj;
        }

        return false;
    }

    public void clearSession(){
        getEntityManager().clear();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Session getSession(){
        return (Session)entityManager.getDelegate();
    }

}
