package com.igorlinnik.repository;


import com.igorlinnik.dominio.SituacaoUsuario;
import com.igorlinnik.dominio.Usuario;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Escla interface conterá métodos implementados pelo Spring Data JPA ou atravé de @Query
 */
@Repository
public interface UsuarioRepository extends CustomJpaRepository<Usuario, String> {
    Optional<Usuario> findByLogin(String login);
    List<Usuario> findBySituacao(SituacaoUsuario situacaoUsuario);

}
