package com.igorlinnik.repository;

/**
 * Esta classe conterá os métodos que serão implementados por nós!
 * Não contém métodos com implementações fornecidas pelo Spring Data JPA
 */
public interface UsuarioRepositoryQuery {
}
