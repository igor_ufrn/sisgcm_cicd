package com.igorlinnik.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Nesta classe devem constar as implementações customizadas de nossas consultas relacionadas aos usuários!
 */
public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {

    @PersistenceContext
    EntityManager em;

}
