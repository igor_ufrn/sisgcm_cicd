package com.igorlinnik.repository;

import org.hibernate.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import java.util.Date;

@NoRepositoryBean
public interface CustomJpaRepository<T, ID> extends JpaRepository<T,ID> {
    void detach(T entity);
    void clearSession();
    EntityManager getEntityManager();
    Session getSession();


    String parseString(Object obj);
    Character parseCharacter(Object obj);
    Long parseLong(Object obj);
    Short parseShort(Object obj);
    Double parseDouble(Object obj);
    Integer parseInteger(Object obj);
    Date parseDate(Object obj);
    Boolean parseBoolean(Object obj);
}
