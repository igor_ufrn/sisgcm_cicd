package com.igorlinnik.validation;

import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;

import java.util.List;

public class ValidatorUtil {


    public static void verificarCompatibilidadeMediaType(MediaType mediaType,
                                                   List<MediaType> mediaTypesAceitas) throws HttpMediaTypeNotAcceptableException {

        boolean compativel = mediaTypesAceitas.stream()
                .anyMatch(mediaTypeAceita -> mediaTypeAceita.isCompatibleWith(mediaType));

        if (!compativel) {
            throw new HttpMediaTypeNotAcceptableException(mediaTypesAceitas);
        }
    }


    public static boolean isLoginValido(String login){
        return login != null && ! login.isEmpty() && login.length() <= 7;
    }


}
