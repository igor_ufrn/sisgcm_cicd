package com.igorlinnik.dto.output;

import com.igorlinnik.dominio.PerfilUsuario;
import com.igorlinnik.dominio.SituacaoUsuario;
import lombok.Data;

@Data
public class UsuarioDtoOutput {
    private String login;
    private String nome;
    private PerfilUsuario perfil;
    private SituacaoUsuario situacao;
}
