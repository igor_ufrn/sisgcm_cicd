package com.igorlinnik.dto.input;

import com.igorlinnik.dominio.PerfilUsuario;
import com.igorlinnik.dominio.SituacaoUsuario;
import lombok.Data;

import javax.validation.constraints.*;


@Data
public class UsuarioDtoInput {

    @Size(min = 1, max = 7)
    @NotBlank
    private String login;
    @NotBlank
    private String senha;
    @NotBlank
    private String nome;
    @NotNull
    private PerfilUsuario perfil;
    @NotNull
    private SituacaoUsuario situacao;
}
