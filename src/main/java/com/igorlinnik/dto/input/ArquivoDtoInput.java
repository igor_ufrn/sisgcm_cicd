package com.igorlinnik.dto.input;

import com.igorlinnik.validation.FileContentType;
import com.igorlinnik.validation.FileSize;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@ApiModel
@Data
public class ArquivoDtoInput {

    @NotNull
    @FileSize(max = "2MB")
    @FileContentType(allowed = {MediaType.ALL_VALUE})
    private MultipartFile arquivo;
    @NotBlank
    private String descricao;


}
