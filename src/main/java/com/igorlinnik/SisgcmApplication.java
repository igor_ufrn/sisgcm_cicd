package com.igorlinnik;

import com.igorlinnik.repository.CustomJpaRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = CustomJpaRepositoryImpl.class)
public class SisgcmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SisgcmApplication.class, args);
    }

}
