package com.igorlinnik.controllers;

import com.igorlinnik.config.web.security.CheckSecurity;
import com.igorlinnik.dominio.SituacaoUsuario;
import com.igorlinnik.dominio.Usuario;
import com.igorlinnik.dto.input.UsuarioDtoInput;
import com.igorlinnik.dto.output.UsuarioDtoOutput;
import com.igorlinnik.repository.UsuarioRepository;
import com.igorlinnik.services.UsuarioService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/usuarios")
public class UsuariosController {

    private UsuarioRepository usuarioRepository;
    private ModelMapper modelMapper;
    private UsuarioService usuarioService;

    public UsuariosController(UsuarioRepository usuarioRepository, ModelMapper modelMapper, UsuarioService usuarioService) {
        this.usuarioRepository = usuarioRepository;
        this.modelMapper = modelMapper;
        this.usuarioService = usuarioService;
    }

    @CheckSecurity.Usuario.PodeConsultar
    @GetMapping("/listar")
    public List<UsuarioDtoOutput> listar() {
        var usuarios = usuarioRepository.findAll();
        if( usuarios != null && !usuarios.isEmpty() ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<UsuarioDtoOutput>();
        }
    }

    @CheckSecurity.Usuario.PodeCadastrar
    @PostMapping("/cadastrar")
    @ResponseStatus(HttpStatus.CREATED)
    public UsuarioDtoOutput cadastrar(@Valid @RequestBody UsuarioDtoInput usuarioDtoInput){
        Usuario usuario = modelMapper.map(usuarioDtoInput,Usuario.class);
        return modelMapper.map(usuarioService.cadastrar(usuario),UsuarioDtoOutput.class);
    }



    @CheckSecurity.Usuario.PodeEditar
    @PutMapping("/alterar")
    public UsuarioDtoOutput alterar(@Valid @RequestBody UsuarioDtoInput usuarioDtoInput){
        Usuario usuarioSalvo = usuarioService.atualizar(modelMapper.map(usuarioDtoInput,Usuario.class));
        return modelMapper.map(usuarioSalvo, UsuarioDtoOutput.class);
    }

    @CheckSecurity.Usuario.PodeExcluir
    @DeleteMapping("/{login}/excluir")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable String login){
        usuarioService.remover(login);
    }

    @CheckSecurity.Usuario.PodeAtivar
    @PutMapping("/{login}/ativar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void ativar(@PathVariable String login){
        usuarioService.ativar(login);
    }

    @CheckSecurity.Usuario.PodeInativar
    @PutMapping("/{login}/inativar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void inativer(@PathVariable String login){
        usuarioService.desativar(login);
    }

    @CheckSecurity.Usuario.PodeAlterarPropriaSenha
    @PutMapping(value = "/{login}/alterarsenha", consumes = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarSenhaUsuarioLogado(@PathVariable String login,  @NotBlank @RequestBody String novaSenha){
        usuarioService.alterarPropriaSenha(login,novaSenha);
    }


    @CheckSecurity.Usuario.PodeConsultar
    @GetMapping("/ativos")
    public List<UsuarioDtoOutput> ativos() {
        var usuarios = usuarioRepository.findBySituacao(SituacaoUsuario.ATIVO);
        if( usuarios != null && !usuarios.isEmpty() ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<UsuarioDtoOutput>();
        }
    }

    @CheckSecurity.Usuario.PodeConsultar
    @GetMapping("/inativos")
    public List<UsuarioDtoOutput> inativos() {
        var usuarios = usuarioRepository.findBySituacao(SituacaoUsuario.INATIVO);
        if( usuarios != null && !usuarios.isEmpty() ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<UsuarioDtoOutput>();
        }
    }

    @CheckSecurity.Usuario.PodeConsultar
    @GetMapping("/{login}")
    public UsuarioDtoOutput usuarioByLogin(@PathVariable @NotBlank String login) {
        Usuario usuario = usuarioService.getUsuarioByLogin(login);
        return modelMapper.map(usuario, UsuarioDtoOutput.class);
    }





}