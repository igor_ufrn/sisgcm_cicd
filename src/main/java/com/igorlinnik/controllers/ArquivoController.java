package com.igorlinnik.controllers;


import com.igorlinnik.dto.input.ArquivoDtoInput;
import com.igorlinnik.exception.types.EntidadeNaoEncontradaException;
import com.igorlinnik.services.ArquivoStorageService;
import com.igorlinnik.exception.types.StorageException;
import com.igorlinnik.validation.ValidatorUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiParam;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;


@RestController
@RequestMapping("/arquivos/")
public class ArquivoController {

    private ArquivoStorageService arquivoStorageService;

    public ArquivoController(ArquivoStorageService arquivoStorageService) {
        this.arquivoStorageService = arquivoStorageService;
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiImplicitParam(dataTypeClass = MultipartFile.class, required = true,  name = "arquivo", allowEmptyValue = false, paramType = "formData")
    public void uploadArquivo(@Valid @ApiParam(required = true) ArquivoDtoInput arquivoDtoInput, @ApiParam(required = true) @NotNull @RequestPart(required = true) MultipartFile arquivo) {

        try {
            arquivoStorageService.armazenar(
                    ArquivoStorageService
                            .NovoArquivo
                            .builder()
                            .nomeAquivo(arquivoStorageService.gerarNomeArquivo(arquivoDtoInput.getArquivo().getOriginalFilename()))
                            .contentType(arquivoDtoInput.getArquivo().getContentType())
                            .inputStream(arquivoDtoInput.getArquivo().getInputStream())
                            .build()
            );

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping(produces = MediaType.ALL_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> downloadArquivo(@RequestParam String nomeArquivoParam, @RequestHeader(name = "accept") String acceptHeader, HttpServletResponse httpServletResponse)
            throws HttpMediaTypeNotAcceptableException {
        try {

            ArquivoStorageService.ArquivoRecuperado arquivoRecuperado = arquivoStorageService.recuperar(nomeArquivoParam);

            MediaType mediaTypeArquivo = null;

            if(arquivoRecuperado.temUrl()) {
                mediaTypeArquivo = MediaType.parseMediaType(arquivoRecuperado.getContentType());
            } else {
                String mimeType = Files.probeContentType(arquivoRecuperado.getPath());
                mediaTypeArquivo = MediaType.parseMediaType(mimeType);
            }

            List<MediaType> mediaTypesAceitas = MediaType.parseMediaTypes(acceptHeader);
            ValidatorUtil.verificarCompatibilidadeMediaType(mediaTypeArquivo, mediaTypesAceitas);


            if (arquivoRecuperado.temUrl()) {
                /*
                return ResponseEntity
                        .status(HttpStatus.SEE_OTHER)
                        .header(HttpHeaders.LOCATION, arquivoRecuperado.getUrl())
                        .build();

                 */
                return ResponseEntity.ok()
                        .contentType(mediaTypeArquivo)
                        .body(new InputStreamResource(arquivoRecuperado.getInputStream()));
            } else {
                return ResponseEntity.ok()
                        .contentType(mediaTypeArquivo)
                        .body(new InputStreamResource(arquivoRecuperado.getInputStream()));
            }

        } catch (EntidadeNaoEncontradaException | IOException | StorageException e) {
            return ResponseEntity.notFound().build();
        }
    }




}
