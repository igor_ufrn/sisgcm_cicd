package com.igorlinnik.services;


import com.igorlinnik.exception.types.StorageException;
import com.igorlinnik.config.storage.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;

import java.nio.file.Files;
import java.nio.file.Path;

public class LocalArquivoStorageService implements ArquivoStorageService {

	@Autowired
	private StorageProperties storageProperties;
	
	@Override
	public ArquivoRecuperado recuperar(String nomeArquivo) {
		try {
			Path arquivoPath = getArquivoPath(nomeArquivo);

			ArquivoRecuperado arquivoRecuperado = ArquivoRecuperado.builder()
					.inputStream(Files.newInputStream(arquivoPath)).path(arquivoPath)
					.build();
			
			return arquivoRecuperado;
		} catch (Exception e) {
			throw new StorageException("Não foi possível recuperar arquivo.", e);
		}
	}
	
	@Override
	public void armazenar(NovoArquivo novoArquivo) {
		try {
			Path arquivoPath = getArquivoPath(novoArquivo.getNomeAquivo());
			
			FileCopyUtils.copy(novoArquivo.getInputStream(),
					Files.newOutputStream(arquivoPath));
		} catch (Exception e) {
			throw new StorageException("Não foi possível armazenar arquivo.", e);
		}
	}
	
	@Override
	public void remover(String nomeArquivo) {
		try {
			Path arquivoPath = getArquivoPath(nomeArquivo);
			
			Files.deleteIfExists(arquivoPath);
		} catch (Exception e) {
			throw new StorageException("Não foi possível excluir arquivo.", e);
		}
	}

	private Path getArquivoPath(String nomeArquivo) {
		return storageProperties.getLocal().getDiretorioArquivos()
				.resolve(Path.of(nomeArquivo));
	}

}
