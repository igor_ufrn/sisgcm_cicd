package com.igorlinnik.services;

import lombok.Builder;
import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public interface ArquivoStorageService {

	ArquivoRecuperado recuperar(String nomeArquivo) throws IOException;
	
	void armazenar(NovoArquivo novoArquivo);
	
	void remover(String nomeArquivo);
	
	default void substituir(String nomeArquivoAntigo, NovoArquivo novoArquivo) {
		this.armazenar(novoArquivo);
		
		if (nomeArquivoAntigo != null) {
			this.remover(nomeArquivoAntigo);
		}
	}
	
	default String gerarNomeArquivo(String nomeOriginal) {
		return nomeOriginal;
		//return UUID.randomUUID().toString() + "_" + nomeOriginal;
	}
	
	@Builder
	@Getter
	class NovoArquivo {
		
		private String nomeAquivo;
		private String contentType;
		private InputStream inputStream;
		
	}
	
	@Builder
	@Getter
	class ArquivoRecuperado {
		
		private InputStream inputStream;
		private String url;
		private Path path;
		private String contentType;

		
		public boolean temUrl() {
			return url != null;
		}
		
		public boolean temInputStream() {
			return inputStream != null;
		}
		
	}
	
}
