package com.igorlinnik.services;


import com.igorlinnik.exception.types.StorageException;
import com.igorlinnik.config.storage.StorageProperties;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;

public class S3ArquivoStorageService implements ArquivoStorageService {

	@Autowired
	private AmazonS3 amazonS3;
	
	@Autowired
	private StorageProperties storageProperties;
	
	@Override
	public ArquivoRecuperado recuperar(String nomeArquivo) throws IOException {
		String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
		
		URL url = amazonS3.getUrl(storageProperties.getS3().getBucket(), caminhoArquivo);
		ObjectMetadata objectMetadata = amazonS3.getObjectMetadata(storageProperties.getS3().getBucket(), caminhoArquivo);
		S3Object s3object = amazonS3.getObject(storageProperties.getS3().getBucket(), caminhoArquivo);



		return ArquivoRecuperado.builder().inputStream(new ByteArrayInputStream(s3object.getObjectContent().readAllBytes()))
				.url(url.toString()).contentType(objectMetadata.getContentType()).build();
	}

	@Override
	public void armazenar(NovoArquivo novoArquivo) {
		try {
			String caminhoArquivo = getCaminhoArquivo(novoArquivo.getNomeAquivo());
			
			var objectMetadata = new ObjectMetadata();
			//Por padrão, o Content-Type é "application/octet-stream". Ao acessar URL do objeto com o browser,
			//o mesmo irá fazer o download do arquivo. Para modificar isto e ser mais específico,
			// setamos o Content-Type do arquivo mais especificamente, permitindo ao browser exibir uma imagem na tela
			//caso o arquivo seja uma imagem, por exemplo.
			objectMetadata.setContentType(novoArquivo.getContentType());
			
			var putObjectRequest = new PutObjectRequest(
					storageProperties.getS3().getBucket(),
					caminhoArquivo,
					novoArquivo.getInputStream(),
					objectMetadata)
					//Permitir que URL do objeto adicionado no bucket da amazon seja acessível publicamente.
					//Se não for informado, seria criado privado.
				.withCannedAcl(CannedAccessControlList.PublicRead);
			
			amazonS3.putObject(putObjectRequest);
		} catch (Exception e) {
			throw new StorageException("Não foi possível enviar arquivo para Amazon S3.", e);
		}
	}

	@Override
	public void remover(String nomeArquivo) {
		try {
			String caminhoArquivo = getCaminhoArquivo(nomeArquivo);

			var deleteObjectRequest = new DeleteObjectRequest(
					storageProperties.getS3().getBucket(), caminhoArquivo);

			amazonS3.deleteObject(deleteObjectRequest);
		} catch (Exception e) {
			throw new StorageException("Não foi possível excluir arquivo na Amazon S3.", e);
		}
	}
	
	private String getCaminhoArquivo(String nomeArquivo) {
		return String.format("%s/%s", storageProperties.getS3().getDiretorioArquivos(), nomeArquivo);
	}

}
