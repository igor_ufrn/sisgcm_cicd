package com.igorlinnik.services;

import com.igorlinnik.config.web.security.CryptoSisgcm;
import com.igorlinnik.dominio.SituacaoUsuario;
import com.igorlinnik.dominio.Usuario;
import com.igorlinnik.exception.types.NegocioException;
import com.igorlinnik.exception.types.UsuarioNaoEncontradoException;
import com.igorlinnik.repository.UsuarioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UsuarioService {

    private UsuarioRepository usuarioRepository;
    private ModelMapper modelMapper;

    public UsuarioService(UsuarioRepository usuarioRepository, ModelMapper modelMapper) {
        this.usuarioRepository = usuarioRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public Usuario cadastrar(Usuario usuario) {
        usuario.validate();
        if(usuarioRepository.findByLogin(usuario.getLogin()).isPresent()) {
            throw new NegocioException("Este login já está sendo utilizado por outro usuário");
        } else {
            usuario.setSenha(CryptoSisgcm.crypto(usuario.getSenha()));
            usuarioRepository.save(usuario);
            return usuario;
        }
    }

    @Transactional
    public Usuario atualizar(Usuario usuario) {
        usuario.validate();
        Usuario usuarioBanco = usuarioRepository.findByLogin(usuario.getLogin())
                .orElseThrow(() -> new UsuarioNaoEncontradoException(String.format("Este usuário não existe: %s", usuario.getLogin())));
        modelMapper.map(usuario,usuarioBanco);
        usuarioBanco.setSenha(CryptoSisgcm.crypto(usuarioBanco.getSenha()));
        usuarioRepository.save(usuarioBanco);
        return usuarioBanco;
    }

    @Transactional
    public void remover(String login) {
        Usuario usuarioBanco = usuarioRepository.findByLogin(login)
                .orElseThrow(() -> new UsuarioNaoEncontradoException(login));
        usuarioRepository.delete(usuarioBanco);
    }

    @Transactional
    public void ativar(String login) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
        usuarioBanco.setSituacao(SituacaoUsuario.ATIVO);
    }

    @Transactional
    public void desativar(String login) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
        usuarioBanco.setSituacao(SituacaoUsuario.INATIVO);
    }

    @Transactional
    public void alterarPropriaSenha(String login, String senha) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
        usuarioBanco.setSenha(CryptoSisgcm.crypto(senha));
    }

    public Usuario getUsuarioByLogin(String login) {
        return usuarioRepository.findByLogin(login)
                .orElseThrow(() -> new UsuarioNaoEncontradoException(String.format("Este usuário não existe: %s", login)));
    }

}
