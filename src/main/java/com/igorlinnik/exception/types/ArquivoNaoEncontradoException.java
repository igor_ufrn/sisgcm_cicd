package com.igorlinnik.exception.types;

public class ArquivoNaoEncontradoException extends EntidadeNaoEncontradaException {

    private static final long serialVersionUID = 1L;

    public ArquivoNaoEncontradoException(String mensagem) {
        super(mensagem);
    }



}
