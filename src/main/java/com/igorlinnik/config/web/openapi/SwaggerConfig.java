package com.igorlinnik.config.web.openapi;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;
import java.util.function.Predicate;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final Contact DEFAULT_CONTACT = new Contact(
            "Suporte", "https://www.caixa.gov.br/", "cedesbr453@caixa.gov.br");

    public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "SISGCM", "No contexto deste sistema, o principal ponto de atenção se refere ao\n" +
            "        processo de controle de versão. Neste contexto, entende-se como controle de\n" +
            "        versão, as ações necessárias que permitam gerenciar durante o processo de\n" +
            "        desenvolvimento de criação ou manutenção de um aplicativo, as diferentes\n" +
            "        versões de um artefato ou grupos de artefatos afetados em um novo release,\n" +
            "        além de proporcionar uma rastreabilidade efetiva entre eles.", "2.0",
            null, DEFAULT_CONTACT,
            null, null, Arrays.asList());

    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<>(Arrays.asList(MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE));


    @Bean
    public Docket api() {

        /*
        Docket to decide what kind of APIs you would want to document.
        In this example, we are documenting all APIs.
        You can filter out APIs you do not want to document with Swagger
        */
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .select()
                .paths(Predicate.not(s -> s.matches("/error")))
                .paths(Predicate.not(s -> s.matches("/actuator.*")))
                .build()
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
                .securitySchemes(Arrays.asList(securityScheme()))
                .securityContexts(Arrays.asList(securityContext()));



    }

    List<RequestParameter> globalRequestParameters(){

        RequestParameterBuilder parameterBuilder = new RequestParameterBuilder()
                .in(ParameterType.HEADER)
                .name("Access-Control-Allow-Origin")
                .required(true)
                .hidden(false)
                .query(param -> param.model(model -> model.scalarModel(ScalarType.STRING)));


        List<RequestParameter> parameters = new ArrayList<>();
        parameters.add(parameterBuilder.build());
        return parameters;
    }



    /**
     * Descreve qual foi a técnica que utilizamos para proteger a API
     * @return
     */
    private SecurityScheme securityScheme(){
        return new OAuthBuilder()
                .name("caixaOauth2")
                .grantTypes(grantTypes())
                .scopes(scopes()).build();
    }

    private List<GrantType> grantTypes(){
        return Arrays.asList(new ResourceOwnerPasswordCredentialsGrant("http://localhost:8001/oauth/token"));
    }

    /**
     * Mapeamento de contexto ao securityScheme
     * @return
     */
    private SecurityContext securityContext(){
        var securityReference = SecurityReference
                .builder()
                .reference("caixaOauth2")
                .scopes(scopes().toArray(new AuthorizationScope[0])).build();

        //Descreve os caminhos da API que estão protegidos por um schema de segurança
        return SecurityContext
                .builder()
                .securityReferences(Arrays.asList(securityReference))
                .forPaths(PathSelectors.any())


                .build();
    }

    private List<AuthorizationScope> scopes(){
        return Arrays.asList(
                new AuthorizationScope("READ","Acesso de Leitura"),
                new AuthorizationScope("WRITE","Acesso de Escrita")
        );
    }
}
