package com.igorlinnik.config.web.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextUtil {

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public String getUsuarioId() {
        if(getAuthentication().isAuthenticated() && !(getAuthentication() instanceof AnonymousAuthenticationToken)) {
            Jwt jwt = (Jwt) getAuthentication().getPrincipal();
            return jwt.getClaim("usuario_id");
        } else {
            return "";
        }
    }

    public String getNomeCompletoUsuario() {
        if(getAuthentication().isAuthenticated() && !(getAuthentication() instanceof AnonymousAuthenticationToken)) {
            Jwt jwt = (Jwt) getAuthentication().getPrincipal();
            return jwt.getClaim("nome_completo");
        } else {
            return "USUARIO NAO AUTENTICADO";
        }
    }

    public boolean usuarioRoot(String idUsuario) {
        String idUsuarioLogado = getUsuarioId();
        return idUsuarioLogado.equals("f625982");
    }

    public boolean permiteAlterarSenha(String idUsuario) {
        String idUsuarioLogado = getUsuarioId();
        return idUsuarioLogado.equals(idUsuario);
    }

    public boolean hasAuthority(String authorityName) {
        return getAuthentication().getAuthorities().stream()
                .anyMatch(authority -> authority.getAuthority().equals(authorityName));
    }

    public boolean temEscopoEscrita() {
        return hasAuthority("SCOPE_WRITE");
    }

    public boolean temEscopoLeitura() {
        return hasAuthority("SCOPE_READ");
    }





}
