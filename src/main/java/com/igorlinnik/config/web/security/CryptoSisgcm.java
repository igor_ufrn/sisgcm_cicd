package com.igorlinnik.config.web.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Classe responsável pelas operações de criptografia de Objetos String utilizadas no Sistema
 * @author CNDESBR452
 */
public class CryptoSisgcm {
    /**
     * Método que realiza o cálculo matemático para criptograr a String passada como parâmetro.
     * @param input
     * @return
     */
    public static String crypto(String input) {
        String stringCrypto = input;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA1");
            md.update(input.getBytes());
            byte[] output = md.digest();
            stringCrypto = bytesToHex(output);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Não foi possível realizar o HASH na entrada de dados");
        }
        return stringCrypto;
    }
    /**
     * Método utilitários para converter bytes em caracteres hexadecimais.
     * @param b
     * @return
     */
    private static String bytesToHex(byte[] b) {
        char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F' };
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
} 