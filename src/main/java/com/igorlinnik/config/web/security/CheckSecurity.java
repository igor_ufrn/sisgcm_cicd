package com.igorlinnik.config.web.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

public @interface CheckSecurity {

	//Namespace de autorizações do recurso Usuario
	@interface Usuario {
		@PreAuthorize("hasAuthority('ADMIN')")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeCadastrar {}

		@PreAuthorize("hasAuthority('ADMIN')")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeConsultar {}

		@PreAuthorize("hasAuthority('ADMIN')")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeEditar {}

		@PreAuthorize("hasAuthority('ADMIN') and @securityContextUtil.usuarioId != #login")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeExcluir {}

		@PreAuthorize("hasAuthority('ADMIN')")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeAtivar {}

		@PreAuthorize("hasAuthority('ADMIN')")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeInativar {}

		@PreAuthorize("isAuthenticated() and @securityContextUtil.usuarioId == #login")
		@Retention(RUNTIME)
		@Target(METHOD)
		@interface PodeAlterarPropriaSenha {}

	}
}
