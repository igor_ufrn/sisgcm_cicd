package com.igorlinnik.config.util;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansUtil {

    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }


}
