package com.igorlinnik.dominio;

import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


@Entity
@Table(name = "gcmtb004_usuario")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario implements PersistDB {



    /**
     * Código Identificador do Usuário.Matrícula do Usuário na CAIXA
     */
    @Id
    @Column(name = "co_usuario")
    @Size(min = 1, max = 7)
    @NotBlank
    private String login;

    /**
     * Senha do usuário
     */
    @Column(name = "de_senha")
    @NotBlank
    private String senha;

    /**
     * Nome do usuário
     */
    @Column(name = "no_usuario")
    @NotBlank
    private String nome;

    /**
     * Descrição do Perfil do Usuário.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "de_perfil")
    @NotNull
    private PerfilUsuario perfil;

    /**
     * Indicador que informa a Situação do Usuário.A - AtivoI - Inativo
     */
    @Column(name = "ic_situacao")
    @NotNull
    private SituacaoUsuario situacao;

    /**
     * Data e Hora em que a Operação de Cadastro, Atualização ou Exclusão do Usuário foi realizada.
     */
    @UpdateTimestamp
    @Column(name = "ts_operacao")
    private LocalDateTime dataOperacao;


}
