package com.igorlinnik.dominio;


import javax.validation.*;
import java.util.Set;

public interface PersistDB {

    default void validate() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PersistDB>> violations = validator.validate(this);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }


}
