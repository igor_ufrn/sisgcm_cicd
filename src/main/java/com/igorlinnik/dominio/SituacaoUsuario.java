package com.igorlinnik.dominio;

import lombok.Getter;

/**
 * Representa os estados em que um usuário pode estar.
 */
@Getter
public enum SituacaoUsuario {

    ATIVO("A"), INATIVO("I");

    /**
     * Representa o código que é armazenado no banco de dados ao persistir a situação do usuário.
     */
    private String code;

    SituacaoUsuario(String code) {
        this.code = code;
    }



}
