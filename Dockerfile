FROM openjdk:11-jdk-slim
ADD target/sisgcm-1.0.0.jar sisgcmWeb.jar
EXPOSE 8061
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "sisgcmWeb.jar"]